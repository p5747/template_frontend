fetch('./annunci.json')
.then(response => response.json())
.then(data => {
    
    
    
    
    console.log(data);
    
    function setCategoryFilter(){
        
        let categoriesWrapper = document.querySelector('#categoriesWrapper')
        
        let categories = Array.from(new Set(data.map(el => el.category)))
        
        categories.forEach(category => {
            
            let div = document.createElement('div')
            div.classList.add('form-check')
            div.innerHTML =
            `
            <input class="form-check-input" type="radio" name="categories" id="${category}">
            <label class="form-check-label" for="${category}">
            ${category}
            </label>
            `
            
            categoriesWrapper.appendChild(div)
            
        })
        
        
    }
    
    setCategoryFilter()
    
    
    function truncateWord(str){

        if(str.length > 10){
           return str.split(' ')[0] + '...'
        } else {
            return str
        }

    }
    
    
    function showCards(array){
        
        let cardWrapper = document.querySelector('#cardWrapper')

        cardWrapper.innerHTML = ''
        
        array.forEach( el => {
            
            let div = document.createElement('div')
            div.classList.add('announcementCard' , 'my-3')
            div.innerHTML =
            `
            <div class="text-center">
            <p class="lead" title="${el.name}">${truncateWord(el.name)}</p>
            <p class="lead">${el.price}$</p>
            </div>
            <p class="lead text-center">${el.category}</p>
            
            `
            
            cardWrapper.appendChild(div)
            
        })
        
        
    }


    showCards(data)
    
    function filterByCategory(){

        let checkRadio = document.querySelectorAll('.form-check-input')

        checkRadio.forEach(el => {

            el.addEventListener('click' , ()=>{

                if(el.id == 'All'){
                    showCards(data)
                } else {

                    let filtered = data.filter(element => element.category == el.id)

                    showCards(filtered)
                }


            })


        })


    }
    
    filterByCategory()



    function priceRange(){

        let maxPrice = data.map(el => Number(el.price)).sort((a,b) => a-b).pop()

        let priceInput= document.querySelector('#priceInput')
        priceInput.max = Math.round(maxPrice)
        priceInput.value = priceInput.max 
        let priceValue = document.querySelector('#priceValue')
        priceValue.innerHTML = ` ${priceInput.max}$ `


        priceInput.addEventListener('input' , ()=> {

            priceValue.innerHTML = ` ${priceInput.value}$ `


        })

    }

    priceRange()
    

    function filterByPrice(){

        let priceInput = document.querySelector('#priceInput')

        priceInput.addEventListener('input' , ()=>{

            let filtered = data.filter(el => Number(el.price) <= priceInput.value)

            showCards(filtered)

        })



    }

    filterByPrice()


    function filterByWord(){

        let inputWord = document.querySelector('#inputWord')

        inputWord.addEventListener('input' , ()=>{

            let filtered = data.filter(el => el.name.toLowerCase().includes( inputWord.value.toLowerCase()   ))

            showCards(filtered)
        })

    }


    filterByWord()

    
})
